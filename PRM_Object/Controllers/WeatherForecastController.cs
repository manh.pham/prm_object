using Impartner.Microservice.PRMReverseGateway.Client.Models;
using Impartner.Microservice.PRMReverseGateway.Client;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System;
using Impartner.Microservice.PRMReverseGateway.Client.Objects.v1;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using PRM_Object;

namespace PRM_Object.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IPRMClientFactory _prmClientFactory;

        public WeatherForecastController(IPRMClientFactory prmClientFactory)
        {
            _prmClientFactory = prmClientFactory;
        }

        [HttpGet("account")]
        public async Task<IActionResult> GetPrmAccount(string env)
        {
            var query = @"SELECT Id AS
                                                AccountId,
                                                Name,
                                                Phone,
                                                IsTest,
                                                IsActive,
                                                MailingLongitude,
                                                MailingLatitude,
                                                MailingPostalCode,
                                                MailingStreet,
                                                MailingCity,
                                                MailingState,
                                                MailingCountry,
                                                Website,
                                                ExternalId1,
                                                ExternalId2,
                                                PrimaryUser.Id,
                                                PrimaryUser.FirstName,
                                                PrimaryUser.LastName,
                                                PrimaryUser.Title,
                                                PrimaryUser.Email,
                                                PrimaryUser.Phone,
                                                PrimaryUser.Mobile,
                                                PrimaryUser.Language
                                                FROM Account where id = 147333";
            var thqlClient = _prmClientFactory.CreateTHQLClient(env);
            var queries = new[]
            {
                new QueryModel
                {
                    Name = nameof(AccountQueryResult.Accounts),
                    Query = query
                }
            };

            var resp = await thqlClient.QueryRequestAsync<AccountQueryResult>(queries, 40);
            var resp2 = await thqlClient.QueryRequestAsync(queries, 40);
            return Ok(resp.Data.Accounts.Data.ToList());
        }

        [HttpGet("account2")]
        public async Task<IActionResult> GetPrmAccount2(string env)
        {
            var queryArguments = new Dictionary<string, string>()
            {
                { "fields", "id,name,PrimaryUser.FirstName" },
                //{"filter",$"id={accountId}" },
                { "take", "1000" }
            };
            var uri = QueryHelpers.AddQueryString($"api/objects/v1/Account", queryArguments);
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            request.Headers.Add("X-PRM-TenantId", $"40");
            var client = _prmClientFactory.Create(env);
            var resp = await client.SendAsync(request);
            var respContent = await resp.Content.ReadAsStringAsync();
            return Ok(respContent);
        }

        [HttpGet("TcmaProgram")]
        public async Task<IActionResult> GetTcmaProgram(string env = "local")
        {
            var query = @"SELECT slug, domain FROM TcmaProgram where programid = 40";
            var thqlClient = _prmClientFactory.CreateTHQLClient(env);
            var queries = new[]
            {
                new QueryModel
                {
                    Name = nameof(QueryResult.Results),
                    Query = query
                }
            };

            var resp = await thqlClient.QueryRequestAsync<QueryResult>(queries, 40);
            return Ok(resp.Data.Results.Data.ToList());
        }

        [HttpGet("TcmaParameter2")]
        public async Task<IActionResult> GetTcmaParameter2(string env)
        {
            var queryArguments = new Dictionary<string, string>()
            {
                { "fields", "id, CreatedBy, RecordLink, user, parametername, displayorder, isexport, Value" },
                //{"filter",$"id={accountId}" },
                { "take", "1000" }
            };
            var uri = QueryHelpers.AddQueryString($"api/objects/v1/TcmaParameter", queryArguments);
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            request.Headers.Add("X-PRM-TenantId", $"40");
            var client = _prmClientFactory.Create(env);
            var resp = await client.SendAsync(request);
            var respContent = await resp.Content.ReadAsStringAsync();
            return Ok(respContent);
        }
    }
}


//SELECT Id, FirstName, LastName,
//     (SELECT username, id, CreatedBy, RecordLink, user, parametername, displayorder, isexport, Value FROM TcmaParameters)
//FROM User where Id = 358487
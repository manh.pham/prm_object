using Impartner.Microservice.PRMReverseGateway.Client.Models;

namespace PRM_Object
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string? Summary { get; set; }
    }

    public class AccountQueryResult
    {
        public QueryResult<AccountModel> Accounts { get; set; }
    }

    public class AccountModel
    {
        public long AccountId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool IsTest { get; set; }
        public bool IsActive { get; set; } = true;
        public string MailingLongitude { get; set; }
        public string MailingLatitude { get; set; }
        public string MailingPostalCode { get; set; }
        public string MailingStreet { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingCountry { get; set; }
        public string Website { get; set; }
        public string ExternalId1 { get; set; }
        public string ExternalId2 { get; set; }
        public User PrimaryUser { get; set; }
        public IList<User> Users { get; set; } = new List<User>();
    }

    public class User
    {
        public long? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public int? LanguageId { get; set; }
        public IList<TcmaParameter> TcmaParameters { get; set; } = new List<TcmaParameter>();
    }

    public class TcmaParameter
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string ParameterName { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsExport { get; set; }
        public string Value { get; set; }
    }

    public class ResultModel
    {
        public string Slug { get; set; }
        public string Domain { get; set; }
        public string ProgramName { get; set; }
    }

    public class QueryResult
    {
        public QueryResult<ResultModel> Results { get; set; }
    }
}
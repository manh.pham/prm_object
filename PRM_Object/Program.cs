using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Metadata;
using Impartner.Microservice.PRMReverseGateway.Client.Extensions;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// PRM config
const string clientNames = "PRM";
builder.Services.AddHttpClient(clientNames);
#if DEBUG
    builder.Services.AddPRMClients(clientNames, x => x.BaseAddress = new Uri("http://localhost:5005/"));
#else
    builder.Services.AddPRMClients(clientNames, x => x.BaseAddress = new Uri("http://prm/"));
#endif

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Faker data
var db = new MyContext();
// db.Add(new TcmaProgram
// {
//     ProgramId = 40,
//     Domain = Faker.Internet.Url(),
//     Slug = Faker.Internet.DomainWord()
// });

// var t = new TcmaTheme
// {
//     ProgramId = 40,
//     Description = Faker.Name.FullName(),
//     ExternalCode = Faker.Name.FullName(),
//     IsActive = true
// };
// db.Add(t);
// await db.SaveChangesAsync();
// var cat = new TcmaCategory
// {
//     Id = 1,
//     ProgramId = 40,
//     Name = Faker.Country.Name(),
//     Description = Faker.Name.FullName(),
//     Active = true,
//     IsSubscribable = true,
//     DisplayOrder = 1,
//     CreatedUtc = new DateTime(),
//     ModifiedUtc = new DateTime()
// };
// cat.TcmaCategoryTranslations.Add(new TcmaCategoryTranslation
// {
//     Name = Faker.Country.Name(),
//     Description = Faker.Name.FullName(),
//     Language = "English"
// });
// cat.TcmaCategoryTranslations.Add(new TcmaCategoryTranslation
// {
//     Name = Faker.Country.Name(),
//     Description = Faker.Name.FullName(),
//     Language = "German"
// });
// var cat2 = new TcmaCategory
// {
//     Id = 2,
//     ProgramId = 40,
//     Name = Faker.Country.Name(),
//     Description = Faker.Name.FullName(),
//     Active = true,
//     IsSubscribable = true,
//     DisplayOrder = 1,
//     CreatedUtc = new DateTime(),
//     ModifiedUtc = new DateTime()
// };
// cat2.TcmaCategoryTranslations.Add(new TcmaCategoryTranslation
// {
//     Name = Faker.Country.Name(),
//     Description = Faker.Name.FullName(),
//     Language = "English"
// });
// cat2.TcmaCategoryTranslations.Add(new TcmaCategoryTranslation
// {
//     Name = Faker.Country.Name(),
//     Description = Faker.Name.FullName(),
//     Language = "German"
// });
// db.Add(cat);
// db.Add(cat2);
// db.Add(new TcmaCategoryTheme
// {
//     CategoryId = cat.Id,
//     ThemeId = 8
// });
// db.Add(new TcmaCategoryTheme
// {
//     CategoryId = cat2.Id,
//     ThemeId = 8
// });
// await db.SaveChangesAsync();



app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();


public class MyContext : DbContext
{
    public DbSet<TcmaProgram> TcmaPrograms { get; set; }
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlServer("user id=ResellerView;password=TerfindorTaxid1;initial catalog=zzResellerView_augment-team-master;data source=featuresql.treehousei.net;Connect Timeout=30;Type System Version=SQL Server 2012;Trusted_Connection=True;TrustServerCertificate=True;Integrated Security=false");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TcmaCategory>()
            .HasMany(e => e.TcmaCategoryTranslations)
            .WithOne(e => e.TcmaCategory)
            .HasForeignKey(e => e.CategoryId)
            .IsRequired();
        modelBuilder.Entity<TcmaCategory>()
            .HasMany(e => e.TcmaCategoryThemes)
            .WithOne(e => e.TcmaCategory)
            .HasForeignKey(e => e.CategoryId)
            .IsRequired();
        modelBuilder.Entity<TcmaTheme>()
            .HasMany(e => e.TcmaCategoryThemes)
            .WithOne(e => e.TcmaTheme)
            .HasForeignKey(e => e.ThemeId)
            .IsRequired();
        modelBuilder.Entity<TcmaCategoryTheme>().HasKey(c => new { c.CategoryId, c.ThemeId });
    }
}

[Table("TcmaProgram")]
public class TcmaProgram
{
    [Key]
    public int ProgramId { get; set; }

    public string Slug { get; set; }

    public string Domain { get; set; }
}

[Table("TcmaTheme")]
public class TcmaTheme
{
    public int Id { get; set; }
    public int ProgramId { get; set; }

    public string Description { get; set; }

    public string ExternalCode { get; set; }

    public bool IsActive { get; set; }
    public IList<TcmaCategoryTheme> TcmaCategoryThemes { get; set; } = new List<TcmaCategoryTheme>();
}

[Table("TcmaCategoryTheme")]
public class TcmaCategoryTheme
{
    public int ThemeId { get; set; }
    public int CategoryId { get; set; }
    public TcmaTheme TcmaTheme { get; set; }
    public TcmaCategory TcmaCategory { get; set; }
}

[Table("TcmaCategory")]
public class TcmaCategory
{
    public int Id { get; set; }

    public int ProgramId { get; set; }
    public int? ParentId { get; set; }

    public string Name { get; set; }
    public string Description { get; set; }
    public bool Active { get; set; }
    public bool IsSubscribable { get; set; }
    public int DisplayOrder { get; set; }


    public DateTime CreatedUtc { get; set; }
    public DateTime ModifiedUtc { get; set; }
    public IList<TcmaCategoryTranslation> TcmaCategoryTranslations { get; set; } = new List<TcmaCategoryTranslation>();
    public IList<TcmaCategoryTheme> TcmaCategoryThemes { get; set; } = new List<TcmaCategoryTheme>();
}

[Table("TcmaCategoryTranslation")]
public class TcmaCategoryTranslation
{
    public int Id { get; set; }

    public int CategoryId { get; set; }

    public string Name { get; set; }
    public string Description { get; set; }
    public string Language { get; set; }
    public TcmaCategory TcmaCategory { get; set; }
}

